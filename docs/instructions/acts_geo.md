# Importing Detector Geometry to ACTS
Step by step instructions for importing the Muon Collider detector geometry into ACTS.

Reference: [ACTS Material Mapping Tutorial](https://acts.readthedocs.io/en/v15.0.0/examples/howto/run_material_mapping.html)

The [ACTSFun](https://gitlab.cern.ch/berkeleylab/MuonCollider/ACTSFun) workspace contains helpful tools for creating a Muon Collider geometry that can be used by ACTS. The following instructions are based on a working setup of it.


## Running the Geantino Scan
The geantino scan extracts the material (radiation lengths) from the detector geometry. There is an ACTS example program that can perform this scan on any DD4hep geometry. An explicit definition of the tracking volume is not required.

After editing `response/geantinoscan.response` to point to the DD4hep definition of your geometry, run the geantino scan:
```shell
${MYBUILD}/exts/acts/bin/ActsExampleMaterialRecordingDD4hep --response-file ${MYWORKSPACE}/response/geantinoscan.response
```

The output geantiono scan will be stored in `geant4-material-tracks.root`.

## Converting DD4hep into TGeo
ACTS's DD4hep plugin requires the DD4hep detector tree to be structured in a specific way to correctly idenfity the logical components (ie: layers). The MCC DD4hep description does not follow this conversion. Thus the first step is to export the MCC DD4hep geometry into TGeo. ACTS's TGeo plugin is more "smart" about extracting the logical information.

General code for converting DD4hep to TGeo is available in the [ACTSMCC](https://gitlab.cern.ch/berkeleylab/MuonCollider/ACTSMCC) package as the `dd2tgeo` tool.

```shell
${MYBUILD}/packages/ACTSMCC/dd2tgeo exts/detector-simulation/geometries/MuColl_v1/MuColl_v1.xml MuColl_v1.root
```

## Validating Geometry
The MCC geometry can be validated by using the Fatras module to simulate hits in the tracking detector and visualize the results.

The `tgeo-lctracker.response` response file contains all the code necessary to load the TGeo MCC detector into ACTS. Make sure to edit it to update the path to your new TGeo ROOT file.

Run the following command to generate 1000 single muon events. The output will be stored in `hits.root`.
```shell
${MYBUILD}/exts/acts/bin/ActsExampleFatrasTGeo --response-file ${MYBUILD}/response/tgeo-lctracker.response --output-root --events 1000
```

The resulting hits file can be viewed using the `study_geometry.py` script.
```shell
python ${MYWORKSPACE}/python/study_geometryid.py
```

It should look something like the following.
![Volume ID](/analysis/muoncollider/tracking/actsgeometry/volume_id.png)
![Layer ID](/analysis/muoncollider/tracking/actsgeometry/layer_id.png)
![Layer ID, Barrel](/analysis/muoncollider/tracking/actsgeometry/layer_id_xy.png)

## Performing Material Map
This steps maps the geantino scan onto the volumes/surfaces of the tracking geometry.

1. Convert the geometry into a JSON file (`geometry-map.json`).
```shell
${MYBUILD}/exts/acts/bin/ActsExampleGeometryTGeo -n1 -j1 --mat-output-file geometry-map  --response-file ${MYBUILD}/response/tgeo-lctracker.response  --output-json true --mat-output-allmaterial true --mat-output-sensitive false
```

2. Convert the geometry JSON file into an easier to edit JSON file (`config-map.json`).
```shell
python3 ${MYWORKSPACE}/exts/acts/Examples/Scripts/MaterialMaping/writeMapConfig.py geometry-map.json config-map.json
```

3. Edit the `config-map.json` to choose which surfaces/volumes should have matterial mapped to them. This is done by changing the `materialMap` value to `true`. 

4. Convert the updated `config-map.json` back to `geometry-map.json`.
```shell
python3 ${MYWORKSPACE}/exts/acts/Examples/Scripts/MaterialMaping/configureMap.py geometry-map.json config-map.json
```

5. Calculate the material map. The result will be stored in `material-maps.root`.
```shell
${MYBUILD}/exts/acts/bin/ActsExampleMaterialMappingTGeo -j1 --input-root true --input-files geant4_material_tracks.root --mat-input-type file --mat-input-file geometry-map.json --output-root --output-json --output-cbor --mat-output-file material-maps --mat-mapping-surfaces true --mat-mapping-volumes true --mat-mapping-volume-stepsize 1 --response-file ${MYBUILD}/response/tgeo-lctracker.response
```

## Validate Material Map
The validation step performs a geantino scan using the mapped material. The results are then compared against the scan done using Geant4. If the material map is correct, then the two should agree.

1. Run the geantino scan of the mapped material.
```shell
${MYBUILD}/exts/acts/bin/ActsExampleMaterialValidationTGeo -n 1000 --mat-output-file val-mat-map --output-root true --prop-stepper 0 --response-file ${MYBUILD}/response/tgeo-mat-lctracker.response
```

2. Make validation plots using ACTS' ROOT macros.
```shell
mkdir Validation

root -q -b ${MYWORKSPACE}/exts/acts/Examples/Scripts/MaterialMaping/Mat_map.C'("propagation-material.root","material-maps_tracks.root","Validation")'

mkdir -p Surfaces/prop_plot
mkdir -p Surfaces/map_plot
mkdir -p Surfaces/ratio_plot
mkdir -p Surfaces/dist_plot
mkdir -p Surfaces/1D_plot

root -q -b ${MYWORKSPACE}/exts/acts/Examples/Scripts/MaterialMaping/Mat_map_surface_plot_ratio.C'("propagation-material.root","material-maps_tracks.root","geometry-map.json",100000,"Surfaces/ratio_plot","Surfaces/prop_plot","Surfaces/map_plot")'
root -q -b ${MYWORKSPACE}/exts/acts/Examples/Scripts/MaterialMaping/Mat_map_surface_plot_dist.C'("material-maps_tracks.root","geometry-map.json",-1,"Surfaces/dist_plot")'
root -q -b ${MYWORKSPACE}/exts/acts/Examples/Scripts/MaterialMaping/Mat_map_surface_plot_1D.C'("material-maps_tracks.root","geometry-map.json",100000,"Surfaces/1D_plot")'
```
