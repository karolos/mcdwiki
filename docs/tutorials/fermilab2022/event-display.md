# Event Display
To display events use the package [CEDViewer](https://github.com/iLCSoft/CEDViewer):

```bash
ced2go -d /opt/ilcsoft/muonc/detector-simulation/geometries/MuColl_v1/MuColl_v1.xml Output_REC.slcio
```

