# Reconstruction with Realistic Beam-Induced Background

To obtain a realistic representation of a collision event it is necessary to add Beam Induced Background (BIB) to the signal event.

BIB particles are first generated in FLUKA software and then their interaction with the detector is simulated in GEANT4, exactly like for signal events, as described in the [Simulation](simulation.md) section.
The resulting final-state particles (`MCParticle`) and simulated hits (`SimHit`) are saved to LCIO files.

For this tutorial BIB particles from both muon beams participating in the bunch crossing (BX) were stored to a single file internally split into `1499` events.
In folder `/data/samples/MuCollv1_25ns_nEkin150MeV_QGSPBERT_merged/` you can find 5 different files representing 5 independent bunch crossings simulated with `ddsim`.
The number of events representing 1 BX is defined by the batch-processing configuration used to simulate these BIB samples and might be different when using another BIB sample.

Merging of signal and BIB is done at the level of SimHits, such that they are passed through the digitization step altogether, as would happen in a real experiment.
This is done by the dedicated Marlin processor `OverlayTimingGeneric`, which is the most efficient among several others collected in the [Overlay](https://github.com/MuonColliderSoft/Overlay/tree/master/src) repository.

Several instances of this processor are defined in the `MuC-Tutorial/reconstruction/steer_reco_Hbb.xml` steering file to provide different options:

- `OverlayFalse` - doesn't read any BIB data by defining `/dev/null` as the input file
- `OverlayBIB` - reads exactly `1499` events from one of the BIB files (corresponds to 1 full BX)
- `OverlayTest` - reads only `150` events from one of the BIB files (corresponds to 10% of the full BX)

By default the `OverlayFalse` instance is executed, which is defined by the `Overlay` parameter of the `Config` module and the following sequence of conditional statements:
```xml
<if condition="Config.OverlayFalse">
  <processor name="OverlayFalse"/>
</if>
<if condition="Config.OverlayTest">
  <processor name="OverlayTest"/>
</if>
<if condition="Config.OverlayBIB">
  <processor name="OverlayBIB"/>
</if>
```

### Including 10% of BIB

You can set `Overlay` parameter to `Test` for including 10% of BIB in each signal event.
Setting `NumberBackground` parameter of the `OverlayTest` processor to a larger value will increase the fraction of BIB added to the event.
This process is computationally intensive and quite slow, therefore it is recommended to only process 1 signal event in this mode by setting `MaxRecordNumber` to `1` in the `<global>` section.

Note that including 100% of BIB you will most likely exceed the memory limit of your computing machine, therefore it is not advised during the tutorial.
Yet this limitation can be overcome using the workaround discussed in one of the [Advanced topics](`overlay.md`).

After finishing the reconstruction of a single event you can examine the contents of the output file, which now should have many more hits in all the subdetectors due to the BIB contribution.

### BIB suppression during reconstruction

The number of tracks reconstructed with BIB included in the event will be much higher, most of which will be fake tracks with bad Chi2.
As a counter-measure you can enable additional filtering processors for the Vertex Detector collections, which are defined in the `subconfigs/TrackerDigitization.xml` file, by adding them to the `<execute>` section of the steering file after Digitization and before Tracking processors, like this:
```xml
<processor name="FilterDL_VXDB"/>
<processor name="FilterDL_VXDE"/>
```

These processors select only pairs of hits on the double layers of the Vertex Detector that are aligned with the interaction point, which rejects the majority of BIB hits.

To take advantage of the filtered hits it is also necessary to point the `CKFTracking` processor to the new filtered collections, like this:
```xml
<parameter name="TrackerHitCollectionNames" type="StringVec" lcioInType="TrackerHitPlane">
  VXDTrackerHits_DLFiltered
  VXDEndcapTrackerHits_DLFiltered
  ITrackerHits
  ITrackerEndcapHits
  OTrackerHits
  OTrackerEndcapHits
</parameter>
```

With fewer hits the track reconstruction will runs much faster and the amount of fake tracks reconstructed from BIB hits is greatly reduced.
