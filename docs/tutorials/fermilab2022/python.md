# Analysis in Python

The LCIO data format has Python bindings called [`pylcio`](https://github.com/MuonColliderSoft/LCIO/tree/master/src/python), which allows to read and write objects using Python scripts.
Such analysis is slower to run than a full-blown Marlin processor, but makes the development process much faster and more flexible.

You can find an example python script in `MuC-Tutorial/analysis/pylcio/`, which can be executed as:
```bash
python run.py <input_file.slcio> -o <output_file.root>
```

The `run.py` script simply provides the handling of command-line parameters and imports a so-called driver, which is executed over each event of the input file, like a C++ processor would.
By default the `CalHitsMCPDriver` is imported, which performs matching of calorimeter hits to their corresponding MCParticles and stores data for each hit into a ROOT `TTree`.
The `TrkHitsMCPDriver` drived can be used instead for a similar analysis of the tracker hits.

Each driver class follows the familiar structure with standard callbacks `def __init__()`, `def startOfData()`, `def processEvent()` and `def endOfData()`.
While the presented examples store data into a `TTree` that needs further analysis later, it is also possible to do more elaborate analysis directly in the driver or even doing it interactively by putting using python bindings directly in a Jupyter notebook.

You can also use the `set_trace()` debugging function anywhere inside the `processEvent()` callback to pause the event loop at a certain point and examine the event content interactively.
