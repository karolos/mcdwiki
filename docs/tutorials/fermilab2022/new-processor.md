# Creating a Marlin Processor
A template Marlin package with a skelton processor is available at [TemplatePackage](https://github.com/MuonColliderSoft/TemplatePackage). It contains the following:

- A template structure for organizing your code.
- A `CMakeLists.txt` for compilation.
- A skelton processor that you can modify.

Note that official Marlin packages follow a slightly different template. This is because they are based on old CMake conventions. The template documented here has been updated for modern CMake.

See the [Git Workspace tutorial](workspace.md) for tools to manage multiple custom packages and share a working setup with your colleagues.xs

## Compiling
Start by making a fork or directly cloning the template repository.
```shell
git clone https://github.com/MuonColliderSoft/TemplatePackage.git
```

The package can be compiled by loading the Muon Collider software environment and compiling as any CMake package.
```shell
source /opt/ilcsoft/muonc/init_ilcsoft.sh
cmake -S TemplatePackage -B build
cmake --build build
```

## Running
The Marlin executable needs to be informed of the location of the library containing your processor. It loads all libraries specified by the `MARLIN_DLL` environmental variable.

The following command will update the variable and call Marlin with the provided example steering file.
```shell
export MARLIN_DLL=$(realpath build/libTemplatePackage.so):${MARLIN_DLL}
Marlin --global.LCIOInputFiles=$(realpath ~/MuonCollider/data/muonGun_sim_MuColl_v1.slcio) share/example.xml
```

## Modifying
The processor is implemented inside the `src/TemplateProcessor.cxx`, with the header located in `TemplatePackage/TemplateProcessor.hxx`.

The class implements the following functions:

- `init()`: Called at the start of Marlin execution.
- `processRunHeader()`: Called at start of a file.
- `processEvent()`: Called for each event.
- `end()`: Called at the end.

Paremters are declared inside the class constructor using the following template:
```c++
registerProcessorParameter("MinPt", // Name of the parameter
                           "Minimum particle pT.", // Description
                           _minPt, // Variable that will contain the parameter
                           _minPt); // Default value
```

Parameters that correspond to input and output collections are defined slightly differently.

To define a parameter containing an input collection name:
```c++
registerInputCollection( LCIO::MCPARTICLE,
                         "InputCollectionName" ,
                         "Name of an input collection.",
                         _inputCollectionName,
                         _inputCollectionName
                         );
```

To define a parameter containing an output collection name:
```c++
registerOutputCollection( LCIO::MCPARTICLE,
                          "OutputCollectionName" ,
                          "Name of the output collection" ,
                          _outputCollectionName,
                          _outputCollectionName
                          );
```