# Event Generation
Event generation can be done with any program. Events saved in HepMC3 format can then be used as inputs in the simulation step.

One of the generators used by other future experiments is WHIZARD.
At this [link](https://github.com/MuonColliderSoft/MuC-Tutorial/tree/master/generation)  you can find instructions and the input file used to generate Higgs to b bbar at 3 TeV CoM energy.

Madgraph5_aMC@NLO is another common choice. You can find a complete tutorial [here](https://indico.cern.ch/event/555228/sessions/203428/attachments/1315471/1970459/tutorial-CMSandATLAS-2016.pdf).