# Simulation

The simulation program is part of the [DD4Hep](https://dd4hep.web.cern.ch/dd4hep/) package, which uses as main components the [ROOT](http://root.cern.ch/) geometry package, which is used for construction and visualization of geometry, and the [Geant4](http://geant4.cern.ch/) simulation toolkit, which can be interfaced via DD4hep to perform detector simulation in complex detector designs.

In the `MuC-Tutorial/simulation/` directory you can find the examples configuration file to be used by the simulation command: `ddsim`

To go to the directory use the command line:

```bash
cd ~/MuC-Tutorial/simulation/
```

All the options can be passed as arguments to the command line, but probably the easiest way is to set everything in the steering file and then invoke for example:

```bash
ddsim --steeringFile steer_sim_Hbb.py > sim.log 2>&1
```


Looking in the configuration file you can see for example:

```python
SIM.compactFile = "/opt/ilcsoft/muonc/detector-simulation/geometries/MuColl_v1/MuColl_v1.xml"  # ← path to the geometry definition
SIM.inputFiles = ["/data/samples/HH/mumu2H2bb750.stdhep"] # ← Input file
SIM.outputFile = "mumu_H_bb.slcio" # ← Output file
SIM.numberOfEvents = 10 # ← Number of events to be simulated
```
Use the output of the command `ddsim -h` to see all the other parameters that you can set.


# Simulating single particles:

It is sometimes useful for performance studies to simulate events with single particles (muons, pions, electrons etc) with specific parameters. To learn how to do this, you can look at the configuration file `MuC-Tutorial/simulation/advanced/steer_sim_muGun.py`. In this file you can specifically set the following parameters:

```python
SIM.enableGun = True
```

You can also set parameters of the particle i.e. type, energy, distribution in theta and phi. You can also decide if you want just one particle per event or more. For example:

```python
SIM.gun.particle = "mu-" # muon
SIM.gun.energy = 10.0*GeV  #fixed energy at 10 GeV
SIM.gun.distribution = "uniform"  # flat in theta distribution    

SIM.gun.isotrop = True. #isotropic distribution in Phi
SIM.gun.multiplicity = 1 # one muon per event
SIM.gun.phiMax = None
SIM.gun.phiMin = None
SIM.gun.thetaMax = 172*deg
SIM.gun.thetaMin = 8*deg
```

You can also define parameters of the beamspot for your particle gun sample, where you provide vertex sigma in `cm`.
To get the correct beamspot position for the COM energy you are trying to simulate, you can refer to this accelerator table: [https://muoncollider.web.cern.ch/design/general-parameters](https://muoncollider.web.cern.ch/design/general-parameters)
```python
SIM.vertexSigma = [0.0, 0.0, 0.0, 0.0]
```

You can then execute it as usual:

```bash
ddsim --steeringFile steer_sim_muGun.py > sim.out 2>&1
```
