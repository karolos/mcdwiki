# Workspace
A recipe for a Git-managed workspace to keep track of your scripts and custom
packages. The features are:

- Auto-discovery of Marlin packages.
- Top-level `CMakeLists.txt` to compile custom packages
- A setup script to build the correct `MARLIN_DLL`.

For instructions on creating custom packages, see the
[Creating a Marlin Processor](new-processor.md) tutorial.

## Template Repository
A template for the workspace is provided at [https://github.com/MuonColliderSoft/TemplateWorkspace](https://github.com/MuonColliderSoft/TemplateWorkspace). Make a fork or clone it
locally to start using it.
```shell
git clone https://github.com/MuonColliderSoft/TemplateWorkspace.git MyWorkspace
```

All commands in the remaineder of this tutorial should be run inside your
workspace.
```shell
cd MyWorkspace
```

## Creating the Environment
The first step is to setup the environment, generate the build files and compile
the workspace. This needs to be only run once. In future sessions, you should
use the `setup.sh` script to load an existing build.

Run the following inside a container with `MyWorkspace` as your current working
directory.
```shell
source /opt/ilcsoft/muonc/init_ilcsoft.sh # Setup ILC software
cmake -S . -B build 
cmake --build build
```

## Adding Packages
It is recommended that all external packages are kept in their own repositories.
This makes it easier to reuse them in different workspaces and share them with
your colleagues. They should be included in your workspace as a git submodule.

In this tutorial, we'll be using a new package called
[TrackPerf](https://github.com/MuonColliderSoft/TrackPerf.git) to create
tracking performance plots. It contains two new processors: `FilterTracks` and
`TrackPerf` to select a subset of tracks (important when running with BIB) and
create histograms useful for efficiency plots.

To add the two packages, run the following in the top-level of your workspace.
```shell
git submodule add https://github.com/MuonColliderSoft/TrackPerf.git packages/TrackPerf
```

The top-level `CMakeLists.txt` will scans the contents of the `packages`
directory and generates the build files for any found packages. It needs to be
run manually to trigger the scan.
```shell
cmake --build -DBOOST_INCLUDEDIR=/usr/include/boost173/  -DBOOST_LIBRARYDIR=/usr/lib64/boost173/
```

Currently a feature/bug of the underlying ILC software setup together with our usage of `add_subdirectory` CMake feature,
results in an error if multiple packages are added.
The error will say that the custom target `unistall` was already defined.
Waiting for a central fix, you can manually fix it editing the `CMakeLists.txt` of your package and adding one `SET` command in the line next to the include of the main ILC software settings:
```cmake
INCLUDE( ilcsoft_default_settings )
SET (_ilcsoft_default_settings_loaded TRUE PARENT_SCOPE)
```

## Setup the Environment
The `setup.sh` script will load the Muon Collider Software, update the
`MARLIN_DLL` environmental variable with any compiles custom packages and define
a few other useful environmental variables:

- `MYBUILD`: absolute path to the build directory.
- `MYWORKSPACE` absolute path to the workspace directory.


It should be run in the following occasions:

- At the start of a new session.
- If a new package was added and compiled (to update `MARLIN_DLL`).

To setup your environment, run

```shell
source setup.sh
```

or if you located your build directory somewhere else

```shell
source setup.sh /path/to/your/build/directory
```

## Running a Steering File
The TrackPerf package contains an example steering file that can be used to produce
tracking performance plots for an existing simulated sample.

For example, to run Conformal Tracking:
```shell
Marlin --global.LCIOInputFiles=muonGun_sim_MuColl_v1.slcio --Config.Tracking=Conformal packages/TrackPerf/share/example.xml
```

Or to run ACTS tracking optimized for the BIB environment:
```shell
Marlin --global.LCIOInputFiles=muonGun_sim_MuColl_v1.slcio --Config.Tracking=ACTS packages/TrackPerf/share/example.xml
```