# BIB overlay

As discussed earlier, `OverlayTimingGeneric` is the primary Marlin processor for adding BIB to the signal event.

Its central feature is the ability to only include hits with TOF-corrected timestamp within a fixed range.
Merging of signal and BIB is done for each `SimHit` collection individually, allowing to define separate time-acceptance regions tailored to each collection, in accordance with time resolutions of the corresponding subdetectors.
The list of collections with corresponding time windows in [ns] are defined in the `Collection_IntegrationTimes` parameter.
This filtering allows to skip at the early stage the hits that would have been anyway discarded during the digitization step as out-of-time contributions, saving RAM and CPU.

This approach has to be used with care since depending on the digitization logics out-of-time `SimHit` might also make a significant contribution to the digitized result, requiring to use wider acceptance windows.
For example this would be relevant when applying realistic digitization of tracker hits that takes into account the dead time of fired pixels.
In this case out-of-time hits from as far as few ns before the bunch crossing would still be important for an accurate representation of the detector occupancy.

### Trimming and merging of BIB collections

Due to the extremely large number of elements in each collection, filtering them for every signal event takes a lot of CPU time.
Another time-consuming process is summing of energy contributions from different particles into the same calorimeter cell during its digitization.

It is therefore more efficient to preprocess BIB files creating a so-called trimmed version with time windows already baked into the hit collections and all contributions to calorimeter cells merged into one.
Such trimmed samples significantly speed up the Overlay process due to the smaller number of elements and will give the same result as long as digitization parameters remain unchanged.

To obtain a trimmed sample you can use a dummy *empty* event as the input, add 100% of BIB following the standard overlay procedure and then store only the relevant collections to the trimmed LCIO file, resulting in about x10 smaller BIB file.

The corresponding Marlin steering file and the dummy input file can be found in `MuC-Tutorial/reconstruction/advanced/bib_trimming/`.
The filtered `SimHit` collections will be written to a single event in the new LCIO file, which can be used for Overlay in the main reconstruction steering file.
