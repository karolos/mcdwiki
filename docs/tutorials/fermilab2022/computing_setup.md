# Computing Setup

The best way to setup the muon collider detector software is through containers.
If you have full access to a machine and would like to install natively the software, then please see these [instructions](https://confluence.infn.it/display/muoncollider/Installation).

Some of the tasks of this tutorial, the ones that involve beam-induced background generation and reconstruction, may require a significant RAM usage (currently up to about 9GB) depending on the specific settings. 

Note that this tutorial will be based on a specific version of the container. A list of available versions and release notes is available [here](https://confluence.infn.it/display/muoncollider/Releases+notes).

You can follow any of the instructions in the following sections to setup the code.
This tutorial was developed using a specific snowmass cluster. See the [dedicated section](#snowmass-connect) for specific instructions to setup the code there.

After you've setup the software, see the [Additional Tutorial Setup](#additional-tutorial-setup) section for additional requirements specific to this tutorial.


## Singularity container
The Muon Collider simulation and reconstruction code is available in a singularity container on CERN's cvmfs:
```
/cvmfs/unpacked.cern.ch/registry.hub.docker.com/infnpd/mucoll-ilc-framework:1.6-centos8
```

A typical way to start the container can look like the following:
```bash
singularity run --bind `echo $HOME` --bind /collab/project/snowmass21/data/muonc:/data /cvmfs/unpacked.cern.ch/registry.hub.docker.com/infnpd/mucoll-ilc-framework:1.6-centos8
```
where in the command above we're imporing the user `HOME` folder inside the container as well as importing a specific path that contains input data files as `/data` folder inside the container. For further options, see the general [singularity documentation](https://docs.sylabs.io/guides/3.5/user-guide/introduction.html).

Then you need to source the environment setting:
```bash
source /opt/ilcsoft/muonc/init_ilcsoft.sh
```

## Docker container
All the software is distributed through a docker container. Images for the Muon Collider software are available at [DockerHub](https://hub.docker.com/r/infnpd/mucoll-ilc-framework).

For generic details about the Docker setup please refer to the [guide](https://docs.docker.com/engine/install/).

A typical docker command to start the muon collider software, after having downloaded the image, will look like the following:
```bash
docker run -ti --rm --env DISPLAY=${DISPLAY} --env USER=${USER} --env HOME=/home/${USER} --user=$(id -u $USER):$(id -g $USER) -v <your working directory>:/home/${USER} -v /cvmfs:/cvmfs -w /home/${USER} -v ${HOME}/.Xauthority:/home/${USER}/.Xauthority --net=host --entrypoint /bin/bash infnpd/mucoll-ilc-framework:1.6-centos8
```

First we define some environment variables used to export the graphical interface: `--env DISPLAY=${DISPLAY} --env USER=${USER} --env HOME=/home/${USER}`. 
We define also the local user as the user logged in the container (otherwise you login as root):  `--user=$(id -u $USER):$(id -g $USER)`.
We mount some local directories inside the container: `-v <your working directory>:/home/${USER} -v /cvmfs:/cvmfs`.
We mount some local directories inside the container: `-v <your working directory>:/home/${USER} -v /cvmfs:/cvmfs`.
With the next option we define the working directory inside the container:  `-w /home/${USER}`.
The last two options tell  the container to share the host’s networking namespace: `-v ${HOME}/.Xauthority:/home/${USER}/.Xauthority --net=host`.

Then you need to source the environment setting:
```bash
source /opt/ilcsoft/muonc/init_ilcsoft.sh
```

## Shifter container
You can convert the docker image to a shifter image using a simple command:
```bash
shifterimg -v pull docker:infnpd/mucoll-ilc-framework:1.6-centos8
```

This only has to be done by one person in the system and the image is usually available to other users as well.

A typical command to then run the image will look like:
```bash
shifter --image=docker:infnpd/mucoll-ilc-framework:1.6-centos8 --volume=/global/cfs/cdirs/atlas/spgriso/MuonCollider/data:/data -- /bin/bash --init-file ${HOME}/scripts/bashrc_muc.sh
```
where the `/global/...` path is a path where data is stored and will be visible as `/data` in the container, and `${HOME}/scripts/bashrc_muc.sh` is your personal bashrc script you'd like to run within the container. For example a good thing to do there is to always setup ilcsoft:
```bash
source /opt/ilcsoft/muonc/init_ilcsoft.sh
```
that otherwise needs to be done manually, similarly to the case of other containers.

## Instructions for specific clusters
In this section we report for convenience instructions for specific computing clusters.

### Snowmass Connect
First, you'll need an account on this cluster. To get one, register at <https://connect.snowmass21.io>. You'll need to edit your profile and add an SSH key for logging into the login node below.

You can then login using ssh:
```bash
ssh login.snowmass21.io #as usual, append your 'your_username@' in case it differs from your local computer account
```

To work efficiently on this cluster, please use the `/work/$USER` folder instead of your home folder. This ensure a faster I/O bandwidth and better user experience for everyone. This is also reflected in the singularity setup below.

You can then load and run the singularity container you can just execute:
```bash
singularity run -B /collab/project/snowmass21/data/muonc:/data -B /work/$USER /cvmfs/unpacked.cern.ch/registry.hub.docker.com/infnpd/mucoll-ilc-framework\:1.6-centos8
```
The "bind" option is used to to mount a working directory `/work/$USER` and the `/data` directory with some commonly useful data.

You can check that the Muon Collider software is properly set up. If everything is OK, you should be able to run the following commands with no error messages:
```bash
cd /work/$USER/
ddsim -h
Marlin -h
```

More information on this cluster is available on its [wiki page](https://maniaclab.uchicago.edu/snowmass-connect-docs/).


## Additional Tutorial Setup
For some parts of the tutorial we will also use code that is available in the [MuC-Tutorial](https://github.com/MuonColliderSoft/MuC-Tutorial.git) repository. Clone it in your working directory after setting up the software.

```bash
git clone https://github.com/MuonColliderSoft/MuC-Tutorial.git
```

