# Digitization and Reconstruction

This second step is done using the command: `Marlin` (Modular Analysis and Reconstruction for the LINear Collider).  
As described in the [official GitHub page](https://github.com/iLCSoft/Marlin) of the program, the idea is that every computing task is implemented as a processor (module) that analyses data in an `LCEvent` and creates additional output collections that are added to the event.  
The framework allows to define the processors (and their order) that are executed at runtime in a simple steering file. Via the steering file you can also define named parameters (string, float, int - single and arrays) for every processor as well as for the global scope.  
By using the framework users don't have to write any code that deals with the IO; they simply write processors with defined callbacks, i.e. `init()`, `processRunHeader()`, `processEvent()`, `end()`, which are called centrally by Marlin at different stages of the event loop.

You can generate an example steering file with a list of **all** the available processors and **all** the available parameters using the following command.

```bash
Marlin -x > steering_sample.xml
```

Note that the resulting `steering_sample.xml` file can be used as a starting point for defining your new Marlin steering file by deleting unnecessary processors and adjusting parameters of the processors you actually want to run.

### Running the H->bb example

To speed up the tutorial you can use the prepared steering file `MuC-Tutorial/reconstruction/steer_reco_Hbb.xml` to run the digitization and reconstruction of the simulated `H -> bb` events.

```bash
Marlin steer_reco_Hbb.xml > reco.log 2>&1
```

To make it work you might need to adjust a few parameters, such as the path to the input file, which is defined by the `LCIOInputFiles` parameter in the `<global>` section of the steering file.
See the [Configuration](reco-conf.md) page for more details about the structure of the steering file.

After successful execution of the Marlin process it will produce three output files:

 - `Output_REC.slcio` - contains all the collections produced by the executed processors;  
 - `Output_DST.slcio` - contains a subset of output collections, which are relevant for later analysis;
 *(exact selection of collections is defined in the `Output_DST` section of the steering file)*
 - `histograms.root` - contains some diagnostics plots and trees (defined in the source code of each processor).

You can now examine the contents of the each file using the relevant tools:

- `.root` files - using the standard `TBrowser` in `root <file>`
- `.slcio` files - using one of the standard LCIO utilities:
    - `lcio_event_counter <file>` - to see the number of events written to the LCIO file
    - `anajob <file>` - list collections stored in each event
    - `dumpevent <file> <event_nr>` - list all objects in a specific event of the file

If the files have some content you can analyze these results using one of the techniques described in the following sections.
