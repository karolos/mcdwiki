# Modifying the Detector Geometry

Current detector geometry used in the example steering files is part of the software container and is stored under `/opt/ilcsoft/muonc/detector-simulation/geometries/MuColl_v1/`, which is also stored in the dedicated [GitHub repository](https://github.com/MuonColliderSoft/detector-simulation/tree/master/geometries/MuColl_v1).
The folder contains the main XML file defining the geometry: `MuColl_v1.xml` and a number of other files, which are included into the main file via `<include>` tags and contain definitions of specific subdetectors.
When running any program in ILCSoft that deals with geometry via DD4hep framework, e.g. `ddsim`, `Marlin`, `ced2go`, it parses all these file and constructs every element of the detector geometry on the fly.
The geometry is then kept in memory and used in every event processed by the program.

The `<define>` section of the `MuColl_v1.xml` file contains a list of named constants that are used throughout the other files to build the geometry.
Other files can have their own `<define>` sections with local constant definitions.
For example, `<constant name="VertexEndcap_z2" value="120*mm"/>` in the `Vertex_o2_v06_01.xml` file defines the position of the 2nd Vertex Endcap disk, which is then used later in the `<detectors>` section to place two rings of Si sensors making up a single double layer.

A simple change of this constant to e.g. `<constant name="VertexEndcap_z2" value="150*mm"/>` will move these two disks by 3cm further away from the interaction point, resulting in the updated detector geometry constructed by `ddsim` or `Marlin` when loading these files next time.

### Practical example

You should never modify the original geometry in order to avoid confusion in the future. Instead, copy the directory of the original geometry into a new folder, e.g.:
```bash
cp -r /opt/ilcsoft/muonc/detector-simulation/geometries/MuColl_v1 ./MuColl_v1_mod1
mv ./MuColl_v1_mod1/MuColl_v1.xml ./MuColl_v1_mod1/MuColl_v1_mod1.xml
```

Now you can modify some constants in `./MuColl_v1_mod1/Vertex_o2_v06_01.xml` to change the position of the endcap disks, which is easiest to modify without affecting other parts of the geometry.
Changing other constants, like radii of barrel layers or dimensions of the disks might require to modify other subdetectors to avoid overlaps of different surfaces.

It is also possible to remove certain components of the detector geometry, if they are not relevant for your study, to speed up the simulation process.
For example, you can comment the `<include>` statements for the ECal, HCal and Yoke subdetectors, making geometry initialization significantly faster.

Now you can run the simulation with the new geometry and output file names passed as command-line parameters:
```bash
ddsim --steeringFile steer_sim_Hbb.py --compactFile=./MuColl_v1_mod1/MuColl_v1_mod1.xml --outputFile=./mumu_H_bb_mod1.slcio
```

