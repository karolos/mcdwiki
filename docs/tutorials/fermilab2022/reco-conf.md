# Marlin steering XML file
Giving a quick look at the steering file you can see that it is an xml file  enclosed in the `<marlin>` ... `</marlin>` tags.
All the configuration can be divided into three main sections:

1) **`execute`** section  - contains an ordered list of processor names.
```xml
<execute>
  <processor name="MyAIDAProcessor"/>
  <processor name="MyTestProcessor"/>
  <processor name="MyLCIOOutputProcessor"/>
</execute>
```
Firstly, the order of the processors is important, since output of one processor can be an input of the following ones.
Secondly, each processor to be executed must be defined somewhere in the steering file.

2) **`global`** section  - contains parameters of the whole Marlin process, such as path to the input file(s) `LCIOInputFiles`, maximum number of events to be processed `MaxRecordNumber`, default verbosity level of all processors `Verbosity`, etc.
```xml
<global>
  <parameter name="LCIOInputFiles"> input_file.slcio </parameter>
  <parameter name="MaxRecordNumber" value="-1" />
</global>
```

3) **`processor`** section - configuration of a particular processor that must have a unique `name` and an existing `type`.
```xml
<processor name="MyLCIOOutputProcessor" type="LCIOOutputProcessor">
  <parameter name="LCIOOutputFile" type="string"> Output_DST.slcio </parameter>
  <parameter name="DropCollectionTypes" type="StringVec">
    SimCalorimeterHit
    SimTrackerHit
  </parameter>
  <parameter name="LCIOWriteMode" type="string" value="WRITE_NEW"/>
  <parameter name="SplitFileSizekB" type="int">1048576 </parameter>
  <parameter name="Verbosity" type="string">WARNING </parameter>
</processor>
```

A processor can be defined either directly in the steering file or in a separate XML file included into the steering file to avoid clutter:
```xml
<include ref="subconfigs/InitDD4hep.xml"/>
<include ref="subconfigs/Overlay.xml"/>
```

Multiple processor instances of the same `type` can be defined with different unique names, e.g. to run the same digitization logics over different input collections with corresonding collection names and resolution parameters:
```xml
<processor name="VXDBarrelDigitiser" type="DDPlanarDigiProcessor">
  <parameter name="SimTrackHitCollectionName" type="string" lcioInType="SimTrackerHit"> VertexBarrelCollection </parameter>
  <parameter name="ResolutionU" type="float"> 0.005 </parameter>
. . . .
<processor name="InnerPlanarDigiProcessor" type="DDPlanarDigiProcessor">
  <parameter name="SimTrackHitCollectionName" type="string" lcioInType="SimTrackerHit"> InnerTrackerBarrelCollection </parameter>
  <parameter name="ResolutionU" type="float"> 0.007 </parameter>
```

If a processor is defined but is not included in the `<execute>` section, it will not be used by Marlin.

In the following, you can find a brief description of what each processor included in `MuC-Tutorial/reconstruction/steer_reco_Hbb.xml` does.

The first three processors are responsible for configuring the Marlin built in processors:
```xml
<processor name="MyAIDAProcessor"/> 
<processor name="EventNumber" /> 
<processor name="Config" />
```
These are configuring global settings that are used in the steering of the other processors, for example setting `<parameter name="Overlay" type="string">False</parameter>` within the Config processor allows to include if statements in the `<execute>` section, such as 
```xml
<if condition="Config.OverlayBIB">
  <processor name="OverlayBIB"/>
</if>
```
The AIDAProcessor instead initialises the ROOT-based outputs, see this [section](histograms.md) for more information.

Any outputs in LCIO format require the inclusion of a dedicated processor of type `LCIOOutputProcessor`. This processor allows to configure the output file name, as well as which collections to retain in the output files. This is particularly useful to reduce the file size if you are using a multi-step processing workflow.
Keep in mind that some derived collections are written as pointers to the original collection, so dropping the original collections might break your code downstream!
Here's an example of how to configure these parameters in the output processor.
```xml
<parameter name="DropCollectionNames" type="StringVec"> VBTrackerHits </parameter>
<parameter name="DropCollectionTypes" type="StringVec"> SimCalorimeterHit </parameter>
<parameter name="KeepCollectionNames" type="StringVec"> MCParticle_Tracks </parameter>
```
Several output processors with different contents can be configured in your steering file.

The `<processor name="InitDD4hep"/>` is responsibile for loading the DD4hep detector geometry description, which will be used by other processors.

Next come the overlay processors `<processor name="OverlayBIB"/>` which are described in a dedicated [section](reco-bib.md).

The next block of processors (of type `DDPlanarDigiProcessor`) is responsible for the digitisation of the tracker hits.
```xml
<processor name="VXDBarrelDigitiser"/>
<processor name="VXDEndcapDigitiser"/>
<processor name="InnerPlanarDigiProcessor"/>
<processor name="InnerEndcapPlanarDigiProcessor"/>
<processor name="OuterPlanarDigiProcessor"/>
<processor name="OuterEndcapPlanarDigiProcessor"/>
```
The digitisation is done via smearing functions that smear the GEANT4 hits with resolutions specified in the processor configuration.
These processors can apply a timing selection on the digitised hits by configuring the following parameters.
```xml
<parameter name="UseTimeWindow" type="bool">true </parameter>
<!--Correct hit times for propagation: radial distance/c-->
<parameter name="CorrectTimesForPropagation" type="bool" value="true"/>
<!--Lower bound of the time window [ns]-->
<parameter name="TimeWindowMin" type="float"> -0.3 </parameter>
<!--Upper bound of the time window [ns]-->
<parameter name="TimeWindowMax" type="float"> 0.3 </parameter>
```
At the time of writing there is no dedicated processor to apply such selection later in the workflow.

Similar processors (of type `DDCaloDigi` and `DDSimpleMuonDigi`) are used to respectively digitise the hits of the calorimeters and the muon system. 
