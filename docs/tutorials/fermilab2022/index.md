# Muon Collider Software Tutorial

This wiki serves the purpose of getting people started with the main aspects of the muon collider detector software. The software suite is heavily based on CLIC's [ILCSoft](https://github.com/iLCSoft), with some custom modifications that are collected in a dedicated [Github area](https://github.com/MuonColliderSoft/).

The main focus is running a full-chain Geant4-based simulation, digitization and reconstruction, as well as making performance studies.
A few more advanced topics are listed separately and can be then approached in any order, depending on your specific interest.

* [Computing Setup](computing_setup.md)
* [Event Generation](evgen.md)
* [Simulation](simulation.md)
* Digitization and Reconstruction
    * [Basics](reco-basics.md)
    * [Closer look into the configuration](reco-conf.md)
    * [Useful tools](reco-tools.md)
    * [Realistic Beam-Induced-Background (BIB)](reco-bib.md)
* Study of Object Performance
    * [Histogramming in Marlin (LCIO input files)](histograms.md)
    * [LCTuple (plain ROOT ntuples)](lctuple.md)
    * [Python Analysis of SLCIO Files](python.md)
* Algorithm Development
    * [Adding a Marlin Processor](new-processor.md)
    * [Tracking Custom Packages](workspace.md)
* Advanced Topics
    * [Modifying the Detector Geometry](geometry.md)
    * [Event Displays](event-display.md)
    * [BIB overlay optimisation](overlay.md)
    * [Running on batch systems](batchsystems.md)
    * [Tweaking particle lifetimes](lifetimes.md)
    * [Developing with VSCode](vscode.md)
    * [Jet reconstruction](jet_reco.md)


## Tutorial events
* [Fermilab Workshop, Dec 16th 2022](https://indico.fnal.gov/event/56615/timetable/){target=_blank}
* [Snowmass '21, Sep 30th 2020](https://indico.fnal.gov/event/45187/timetable/#20200930.detailed){target=_blank}
