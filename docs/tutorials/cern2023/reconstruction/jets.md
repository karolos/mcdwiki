# Jet Reconstruction

Jet reconstruction is one of the most challenging tasks in the Muon Collider environment, since most of the detectors are involved and they are affected by the Beam-Induced Background (BIB). 
With the `MuonColliderSoft` package this task can be performed with success, obtaining a good performace with the state-of-the-art algorithm, but many advancements are still possible.
The jet reconstruction is performed by applying several steps: track reconstruction, track refit and filtering, particle flow and jet clustering. 

**Details on the configuration**

The configuration has been tuned specifically for the jet reconstruction, in order to deal with the large yield of hits produced by the BIB.
The tracking is performed with ACTS, as explained in the previous sections of the tutorial.
After the tracking task, the track refit processor applies requirements based on the number of hits in different tracking modules. The configuration of the track refit is here:
https://github.com/MuonColliderSoft/MuC-Tutorial/tree/master/reconstruction/advanced/jet_reco/confile/Tracking.xml
and the requirements on the number of track hits:
```XML
<parameter name="NHitsCuts" type="StringVec">
1,2 4
3,4 3
</parameter>
```
This means that at least 4 hits are required in the Vertex Detector, and at least 3 hits are required in the inner tracker.

Before entering the particle flow, hits in the calorimeter are filtered in order to remove most of the BIB contamination. The configuration for this is in:
https://github.com/MuonColliderSoft/MuC-Tutorial/tree/master/reconstruction/advanced/jet_reco/confile/Calo.xml
 and the lines are:
```
<!--Threshold for ECAL Hits in GeV-->
<parameter name="ECALThreshold" type="float">0.002 </parameter>
<!--Unit for ECAL Threshold. Can be "GeV", "MIP" or "px". MIP and px need properly set calibration constants-->
<parameter name="ECALThresholdUnit" type="string">GeV </parameter>
```
for ECAL, and
```
<!--Threshold for HCAL Hits in GeV-->
<parameter name="HCALThreshold" type="FloatVec">0.002  </parameter>
<!--Unit for HCAL Threshold. Can be "GeV", "MIP" or "px". MIP and px need properly set calibration constants-->
<parameter name="HCALThresholdUnit" type="string">GeV </parameter>
```
for HCAL. The hit thresholds are set to 2 MeV, that is a pretty high value, and a fraction of signal energy is lost in this way. However it is necessary to make the particle flow algorithm working. A reduction of the thresolds should be studied by exploring different particle flow and jet clustering methods.

The particle flow step is performed by PandoraPF, where tracks and calorimeters hits are combined to reconstruct the final particles. The configuration can be find again here:
https://github.com/MuonColliderSoft/MuC-Tutorial/tree/master/reconstruction/advanced/jet_reco/confile/Calo.xml
in the code section starting with:
```
<processor name="DDMarlinPandora" type="DDPandoraPFANewProcessor">
```
There ara many parameters, but the configuration is pretty standard, and usually loose requirements are applied. Thanks to the previous tracks and calorimeters hits filtering, it can runs smoothly, without any particular prescription.

Finally the jet clustering is performed by the processor FastJetProcessor, that can be found in 
https://github.com/MuonColliderSoft/MuC-Tutorial/tree/master/reconstruction/advanced/jet_reco/confile/VertexJet.xml
The configuration is the following:
```
<processor name="FastJetProcessor" type="FastJetProcessor">
  <parameter name="algorithm" type="StringVec">kt_algorithm 0.5</parameter>
  <parameter name="clusteringMode" type="StringVec"> Inclusive 5</parameter>
  <parameter name="jetOut" type="string" lcioOutType="ReconstructedParticle">JetCaloOut </parameter>
  <parameter name="recParticleIn" type="string" lcioInType="ReconstructedParticle"> SelectedPandoraPFOs </parameter>
  <parameter name="recombinationScheme" type="string">E_scheme </parameter>
</processor>
```
where the kt algorithm with a radius parameter of 0.5 is used.

**Suggestions on jet analysis**

There are several remarks that should be considered when analyzing jets obtained in this way.
First of all an average number of 10-20 fake jets per event is found with this algorithm and with the BIB at 1.5 TeV collisions energy. 
These fake jets are produced by the BIB combinatorial: an effective way to remove them in the analysis is to require at least on track inside the jet, since most of them are artifacts from the calorimeters reconstruction. However more advanced techniques that exploit the jet substructure could be employed to perform the fake jet removal in and efficient way.

A jet energy correction should also be determined in the analysis, since the high calorimeters thresholds remove part of the signal energy (up to 50% in some phase space regions). The jet energy correction should be obtained on a statistical basis by comparing the reconstructed jets with the truth-level jets, and it usually depends from the jet transverse momentum and rapidity.

In order to obtain the truth-level jets for the analysis, the same configuration of the FastJetProcessor should be run, but using Monte Carlo particles as inputs instead of the particle flow output.
