# Including BIB

To obtain a realistic representation of a collision event it is necessary to add Beam Induced Background (BIB) to the signal event.

BIB particles are first generated in FLUKA software and then their interaction with the detector is simulated in GEANT4, exactly like for signal events, as described in the [Simulation](../simulation.md) section.
The resulting final-state particles (`MCParticle`) and simulated hits (`SimHit`) are saved to LCIO files.

Merging of signal and BIB is done at the level of SimHits, such that they are passed through the digitisation step altogether, as would happen in a real experiment.
This is done by the dedicated Marlin processor `OverlayTimingGeneric`, which is the most efficient among several others collected in the [Overlay](https://github.com/MuonColliderSoft/Overlay/tree/master/src) repository.

Several instances of this processor are defined in the `mucoll-benchmarks/digitisation/marlin/digi_steer.xml` steering file to provide different options:

- `OverlayNone` - doesn't read any BIB data by defining `/dev/null` as the input file
- `OverlayFull` - reads exactly `1` event from one of the BIB files (corresponds to 1 full BX - requires special read access)
- `OverlayTest` - reads all events events from a few small BIB files (corresponds to about 1% of the full BX)

By default the `False` value is used for `Config.Overlay` parameter, in which case nothing is executed.

### Including 1% of BIB

For the purpose of the tutorial we will set `Overlay` parameter to `Test` for including only 1% of BIB in each signal event, which is split into 10 files with about 10 events/file for flexibility.
Setting `NumberBackground` parameter of the `OverlayTest` processor to a value smaller than 100 will reduce the fraction of BIB added to the event even further.

We will run the digitisation with BIB in a new folder to not overwrite the previous results obtained without BIB, and we'll process only 5 events:

```bash
# Create the working folder
mkdir digi_Hbb_bib && cd digi_Hbb_bib
# Run digitisation with Overlay of a test BIB sample
Marlin ../mucoll-benchmarks/digitisation/marlin/digi_steer.xml \
--global.LCIOInputFiles="../sim_Hbb/output_sim.slcio" \
--DD4hep.DD4hepXMLFile="$MUCOLL_GEO" \
--Config.Overlay="Test" \
--global.MaxRecordNumber=6
```

> NOTE: Including 100% of BIB you will exceed the memory limit of your computing machine, therefore it is not advised during the tutorial.
> This limitation can be overcome by trimming BIB collection before overlay, as discussed in one of the [Advanced topics](../advanced/overlay.md).

After finishing the digitisation of these few events you can examine the contents of the output file, which now should have many more hits in all the subdetectors due to the BIB contribution:

```bash
anajob -m 1 output_digi_light.slcio
```

which you can compare to the same event without BIB obtained in the earlier stage of the tutorial:

```bash
anajob -m 1 ../digi_Hbb/output_digi_light.slcio
```

After finishing the digitisation we can go back to the main working directory for adjusting the reconstruction step:

```bash
cd ../
```

### BIB suppression during reconstruction

The number of tracks reconstructed with BIB included in the event will be much higher, most of which will be fake tracks with bad Chi2.
As a counter-measure additional filtering processors for the Vertex Detector collections were added to the `<execute>` section of the digitisation step when the `Config.Overlay` is not `False`.
These processors are defined in `mucoll-benchmarks/digitisation/marlin/subconfigs/VertexDoubleLayerFitering.xml` file:
```xml
<if condition="Config.OverlayNotFalse">
  <processor name="FilterDL_VXDB"/>
  <processor name="FilterDL_VXDE"/>
</if>
```

These processors select only pairs of hits in the double layers of the Vertex Detector that are aligned with the interaction point, rejecting the majority of BIB hits.
They take the output collections of the digitisation processors and produce new filtered collections with `_DLFiltered` suffix in the name.

To take advantage of the filtered hits we have to change configuration of the `CKFTracking` processor to use the filtered collections for the Vertex Detector instead of the default ones, defined in:  
`mucoll-benchmarks/reconstruction/marlin/subconfigs/TrackReconstruction.xml`
```xml
<parameter name="TrackerHitCollectionNames" type="StringVec" lcioInType="TrackerHitPlane">
	VXDBarrelHits_DLFiltered   <!-- instead of VXDBarrelHits -->
	VXDEndcapHits_DLFiltered   <!-- instead of VXDEndcapHits -->
	. . .
</parameter>
```

After changing the tracking configuration we can run the reconstruction with BIB in a new folder:

```bash
# Create the working folder
mkdir reco_Hbb_bib && cd reco_Hbb_bib
# Link the PandoraSettings folder
ln -s ../mucoll-benchmarks/reconstruction/marlin/PandoraSettings ./
# Run reconstruction with BIB and filtered Vertex collections
Marlin ../mucoll-benchmarks/reconstruction/marlin/reco_steer.xml \
--global.LCIOInputFiles="../digi_Hbb_bib/output_digi.slcio" \
--DD4hep.DD4hepXMLFile="$MUCOLL_GEO" \
--Config.Overlay="Test" \
--global.MaxRecordNumber=6

```

Now you can check the output from the first event of this reconstruction job:

```bash
anajob -m 1 output_reco_light.slcio
```

which you can compare to the same event without BIB obtained in the earlier stage of the tutorial:

```bash
anajob -m 1 ../reco_Hbb/output_reco_light.slcio
```

You should see an increased number of jets, clusters and PFO objects in the case with BIB overlay.


> If you have time, you can try to run the reconstruction using the default Vertex hit collections, without the double-layer filtering.
> This will increase the execution time and numbers of reconstructed objects, which are mostly fake.
