# Digitisation and reconstruction

This stage is executed under the `Marlin` framework (Modular Analysis and Reconstruction for the LINear Collider).  
As described in the [official GitHub page](https://github.com/iLCSoft/Marlin) of the program, the idea is that every computing task is implemented as a processor (module) that analyses data in an `LCEvent` and creates additional output collections that are added to the event.  
The framework allows to define the processors (and their order) that are executed at runtime in a simple `XML` steering file.
Via the steering file you can also define named parameters (`string`, `float`, `int` - single and arrays) for every processor as well as for the global scope.  
This framework allows users to not write any code that deals with the I/O; they simply write processors with predefined callbacks, i.e. `init()`, `processRunHeader()`, `processEvent()`, `end()`, which are called centrally by `Marlin` at different stages of the event loop.

You can generate an example steering file with a list of **all** the available processors and **all** the available parameters using the following command.

```bash
Marlin -x > steering_sample.xml
```

Note that the resulting `steering_sample.xml` file can be used as a starting point for defining your new Marlin steering file by deleting unnecessary processors and adjusting parameters of the processors you actually want to run.


## Steering files

The digitisation and reoconstruction processes are configured as two independent stages with individual steering files, which are executed one after another:

* `mucoll-benchmarks/digitisation/marlin/digi_steer.xml` for the digitisation step;
* `mucoll-benchmarks/reconstruction/marlin/reco_steer.xml` for the reconstruction step.

This helps to reduce the peak RAM usage of the process and allows to rerun only the relevant stage when tuning a specific algorithm or processor configuration.

> NOTE: Unlike Python-based steering files for `ddsim` with their dynamic access to environment variables, Marlin is configured via static XML files. This means that environment-specific values, such as path to the detector geometry, have to be specified manually, either directly in the steering file or via a command-line parameter.

It is possible to override any parameter of any processor defined in the steering file by specifying it as a command-line parameter:

```bash
Marlin --global.LCIOInputFiles="<input_file>.slcio"  --DD4hep.DD4hepXMLFile="$MUCOLL_GEO" <steering_file>.xml`
```

More information about the structure of `Marlin` steering files is given in [this section](config.md).

## Reconstructing H -> bb events

First create a separate folder for each stage:

```bash
mkdir digi_Hbb reco_Hbb
```

> NOTE: This stage runs significantly faster than GEANT4 simulation, allowing to process more events in short time. You can therefore copy a pre-simulated sample of 100 events to obtain higher statistics:
```bash
cp /cvmfs/muoncollider.cern.ch/datasets/tutorial_20230705/sim_Hbb/output_sim.slcio sim_Hbb/
```


Then run the **digitisation sequence** over all events of the input file with `SimHits`:

```bash
cd digi_Hbb
Marlin ../mucoll-benchmarks/digitisation/marlin/digi_steer.xml \
--global.LCIOInputFiles="../sim_Hbb/output_sim.slcio" \
--DD4hep.DD4hepXMLFile="$MUCOLL_GEO"
```

In order to run the **reconstruction stage** we have to do several preparations first.

**Create a symbolic link** to the folder with settings of the `Pandora` package for particle-flow object reconstruction (PFO):

```bash
cd ../reco_Hbb
ln -s ../mucoll-benchmarks/reconstruction/marlin/PandoraSettings ./
```

> NOTE: Include paths inside the `<include />` tags of the Marlin `XML` configuration files are relative to the root `XML` file.
> Instead, paths inside of `<parameter />` tags are relative to the directory in which Marlin is executed.
> For this reason a symbolic link to the `PandoraSettings` folder is created to provide settings for the `DDMarlinPandora` processor.

**Determine location of `ActsTracking` processor** that contains relevant parts of the default `MuColl_v1` geometry converted to the format required by `ACTS` track-reconstruction framework.

```bash
ACTS_PATH=$(echo $MARLIN_DLL | tr ':' '\n' | grep actstracking | sed "s:/lib.*::")
```

> NOTE: This is a temporary solution.
> In the future this path will be stored in an environment variable, like the path to the compact geometry `$MUCOLL_GEO`.

**Run the reconstruction sequence** overriding the paths to the relevant `ACTS` geometry files:

```bash
Marlin ../mucoll-benchmarks/reconstruction/marlin/reco_steer.xml \
--global.LCIOInputFiles="../digi_Hbb/output_digi.slcio" \
--CKFTracking.MatFile="${ACTS_PATH}/share/ACTSTracking/data/material-maps.json" \
--CKFTracking.TGeoFile="${ACTS_PATH}/share/ACTSTracking/data/MuColl_v1.root" \
--DD4hep.DD4hepXMLFile="$MUCOLL_GEO"
```


Each stage will produce several output files:

 - `output_<digi|reco>.slcio` - contains all the collections produced by the executed processors;  
 - `output_<digi|reco>_light.slcio` - contains a subset of output collections, which are relevant for later analysis;
 - `output_<digi|reco>.root` - contains some diagnostics plots and trees (defined in the source code of each Marlin processor).

## Output examination

You can now examine the contents of the each file using the relevant tools:

- `.root` files - using the standard `TBrowser` in `root <file>`
- `.slcio` files - using one of the standard LCIO utilities:
    - `lcio_event_counter <file>` - to see the number of events written to the LCIO file
    - `anajob <file>` - list collections stored in each event
    - `dumpevent <file> <event_nr>` - list all objects in a specific event of the file

If the files have some content you can analyse these results using one of the techniques described in the following sections.

