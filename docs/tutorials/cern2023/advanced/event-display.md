# Event Display
To display events use the package [CEDViewer](https://github.com/iLCSoft/CEDViewer):

```bash
ced2go -d ${MUCOLL_GEO} output_sim.slcio
```

