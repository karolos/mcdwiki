# Modifying the Detector Geometry

Current detector geometry used in the example steering files is part of the software container and is stored under `$MUCOLL_GEO`, which is also stored in the dedicated [GitHub repository](https://github.com/MuonColliderSoft/detector-simulation/tree/master/geometries/MuColl_v1).
The folder contains the main XML file defining the geometry: `MuColl_v1.xml` and a number of other files, which are included into the main file via `<include>` tags and contain definitions of specific subdetectors.
When running any program in ILCSoft that deals with geometry via DD4hep framework, e.g. `ddsim`, `Marlin`, `ced2go`, it parses all these file and constructs every element of the detector geometry on the fly.
The geometry is then kept in memory and used in every event processed by the program.


### Prepare the new geometry directory and files

You should never modify the original geometry in order to avoid confusion in the future. Instead, copy the directory of the original geometry into a new folder, e.g.:
```bash
cp -r $(dirname $MUCOLL_GEO) MuColl_v1_mod1
mv ./MuColl_v1_mod1/MuColl_v1.xml ./MuColl_v1_mod1/MuColl_v1_mod1.xml 
```

At this point you can modify the files in the directory, to implement a new detector geometry.
You can also run the simulation with the new geometry and output file names passed as command-line parameters:
```bash
ddsim --steeringFile mucoll-benchmarks/simulation/ilcsoft/steer_baseline.py --compactFile=./MuColl_v1_mod1/MuColl_v1_mod1.xml --outputFile=./mumu_H_bb_mod1.slcio
```

The detector geometry can be inspected in details with `teveDisplay`:
```bash
teveDisplay -compact ./MuColl_v1_mod1/MuColl_v1_mod1.xml
```
However this command takes quite a long time to load.  
You can quickly visualize the detector changes by using the event display after simulating one or more events:
```bash
ced2go -d ./MuColl_v1_mod1/MuColl_v1_mod1.xml ./mumu_H_bb_mod1.slcio 
``` 

It is also possible to remove certain components of the detector geometry, if they are not relevant for your study, to speed up the simulation or visualization processes.
For example, you can comment the `<include>` statements in `MuColl_v1_mod1.xml` for the ECal, HCal and Yoke subdetectors, making geometry initialization significantly faster.


### Example: Moving a vertex detector sensor

The `<define>` section of the `MuColl_v1_mod1.xml` file contains a list of named constants that are used throughout the other files to build the geometry.
Other files can have their own `<define>` sections with local constant definitions.

As an example, in `Vertex_o2_v06_01.xml`:
```python
<constant name="VertexEndcap_z2" value="120*mm"/>
```
defines the position of the 2nd Vertex Endcap disk, which is then used later in the `<detectors>` section to place two rings of Si sensors making up a single double layer.

A simple change of this constant to e.g. 
```python
<constant name="VertexEndcap_z2" value="150*mm"/>
```
 will move these two disks by 3 cm further away from the interaction point, resulting in the updated detector geometry constructed by `ddsim` or `Marlin` when loading these files next time.

Now you can modify some constants in `./MuColl_v1_mod1/Vertex_o2_v06_01.xml` to change the position of the endcap disks, which is easiest to modify without affecting other parts of the geometry.
Changing other constants, like radii of barrel layers or dimensions of the disks might require to modify other subdetectors to avoid overlaps of different surfaces.

### Example: Modifying the calorimeter technology

It is possible to modify not only the geometry, but also the detector materials, the active area, the segmentation etc.
In this section we show how the calorimeter technology can be changed with few passages.

As first step we can add a new material in the `MuColl_v1_mod1/materials.xml` file:
```python
    <material name="LeadDiflourite">
        <D type="density" value="7.77" unit="g/cm3"/>
        <composite n="1" ref="PB"/>
        <composite n="2" ref="F"/>
    </material>
```

The layer  definition for the ECAL barrel is in `MuColl_v1_mod1/ECalBarrel_o2_v01_02.xml`. The default one is:
```python
<layer repeat="40" vis="ECalLayerVis">
                <slice material = "TungstenDens24" thickness = "1.90*mm" vis="ECalAbsorberVis" radiator="yes"/>
                <slice material = "G10"            thickness = "0.15*mm" vis="InvisibleNoDaughters"/>
                <slice material = "GroundOrHVMix"  thickness = "0.10*mm" vis="ECalAbsorberVis"/>
                <slice material = "Silicon"        thickness = "0.50*mm" sensitive="yes" limits="cal_limits" vis="ECalSensitiveVis"/>
                <slice material = "Air"            thickness = "0.10*mm" vis="InvisibleNoDaughters"/>
                <slice material = "siPCBMix"       thickness = "1.30*mm" vis="ECalAbsorberVis"/>
                <slice material = "Air"            thickness = "0.25*mm" vis="InvisibleNoDaughters"/>
                <slice material = "G10"            thickness = "0.75*mm" vis="InvisibleNoDaughters"/>
</layer>
```

We can change the number of layers, e.g from 40 to 5 with:
```python
<layer repeat="5" vis="ECalLayerVis">
```
and the composition of the layer:
```python
<layer repeat="5" vis="ECalLayerVis">
                <slice material = "LeadDiflourite" thickness = "40*mm" sensitive="yes" limits="cal_limits" vis="ECalSensitiveVis"/>
                <slice material = "Silicon"        thickness = "1*mm" vis="ECalAbsorberVis"/>
                <slice material = "siPCBMix"       thickness = "3*mm" vis="ECalAbsorberVis"/>
                <slice material = "Air"            thickness = "1*mm" vis="InvisibleNoDaughters"/>
</layer>
```
It is important to include the options `sensitive="yes" limits="cal_limits"` in the slice of active material.
In this case we want to read the energy directly in the interaction with the LeadDiflourite crystal.
A radiator slice can be defined with the `radiator="yes"` option.

As for the vertex detector, many calorimeter parameters are defined in `MuColl_v1_mod1.xml` file.
As an example we can control the cell dimension with the parameter:
```python
<constant name="ECal_cell_size"             value="5.1*mm"/>
```

Many customization parameters are available, but it is important to modify them avoiding overlaps between different surfaces.
