# Tweaking particle lifetimes

The lifetime of an unstable particle within the detector simulation is specified in a dedicated GEANT4 file.
Any lifetime effect that was integrated at the level of the event generation (for example by varying the particle widths in MadGraph) is overwritten by the contents of the GEANT4 file. This means that in general, the SM particles will behave as SM particles, and any extra particle will not be long-lived.

In your simulation steering file, you can select the GEANT4 by changing the following line:
```python
SIM.physics.pdgfile = "/data/particle.tbl"
```
to
```python
SIM.physics.pdgfile = "/path/to/your/personal/particle.tbl"
```

The GEANT4 extra particle pdf file has the following format:
```
//  This table was generated from 
//  http://cmssw.cvs.cern.ch/cgi-bin/cmssw.cgi/CMSSW/SimGeneral/HepPDTESSource/data/Pythia6Table.tbl?rev=1.2&cvsroot=CMSSW&content-type=text/vnd.viewcvs-markup
//  (version 1.2).
//  It is essentially a copy of the pythia particle table, with a few heavy ions
//  The ID numbers and names conform to the 2006 PDG standard.
//
//  This table can be read with the addParticleTable function.
//  Fixed columns are not required 
//  Blank characters in the middle of particle names are illegal
//  and will result in nonsense.
//
// Comments are identified by either "//" or "#", beginning in the first column.
// Comments MUST be in the first column.
//
//       ID name                  chg       mass    total width   lifetime [mm]
//
          1 d                      -1      0.33000     0.00000   0.00000E+00
         -1 d~                      1      0.33000     0.00000   0.00000E+00
          2 u                       2      0.33000     0.00000   0.00000E+00
         -2 u~                     -2      0.33000     0.00000   0.00000E+00
          3 s                      -1      0.50000     0.00000   0.00000E+00
         -3 s~                      1      0.50000     0.00000   0.00000E+00
[...]
```
You can vary the mass, width and lifetime as you please.
For example:
```
 1000024 susy-chi_1^+            3    500.235443    1.00000   2.99792E+01
-1000024 susy-chi_1^-           -3    500.235443    1.00000   2.99792E+01
```
In your next simulation run, GEANT will consider the varied parameters contained in your specified pdg file.

*VERY IMPORTANT NOTE*: the mass of the particle needs to be consistent between what was used in the generation and what is specified in the GEANT4 PDG file. If not, the simulation will run without complaining, but the physics outputs will be broken.