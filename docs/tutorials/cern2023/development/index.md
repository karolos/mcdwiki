# Development workflow

The instructions given in the [Computing setup](../setup.md) section simply load all the compiled software into the current environment by updating the relevant environment variables, such as `PATH`, `LD_LIBRARY_PATH`, etc., such that files of all the executables and shared libraries can be located.

Regardless of whether you are running the installation from CVMFS or from a Docker/Apptainer container, the compiled files cannot be modified in a persistent manner. Thus to modify any package a new version of the corresponding code has to be built in a separate folder, where the user has the write permissions.
After that the corresponding environment variables have to be updated to point to the newly built package.

### General code development

In general the whole software stack is built using the Spack package manager, as described in the corresponding [mucoll-spack](https://github.com/MuonColliderSoft/mucoll-spack) repository.
This method allows to rebuild any piece of the software stack from the ground up, but is quite time-consuming, since it requires multiple preperatory steps:

* installation of Spack;
* setup of the code repositories;
* configuration of the development environment;
* rebuilding of the modified package;
* rebuilding of all the packages that depend on it.


### Marlin-processor development

The most frequent need for code development is limited to modifying an existing Marlin processor or adding a new one.
In this case a faster approach is available, as described in the [corresponding section](new-processor.md).
