# Event generation
Our simulation framework supports `HepMC3` and `stdhep` data formats for the input of signal events from Monte Carlo event generators.
As an example we will use a `Whizard` event generator that is included in the software release.

### Generating H -> bb sample in Whizard

A reference configuration file for generating a `HepMC3` sample of `mu+ mu- -> H + neutrinos -> bb + neutrinos` process is available at `mucoll-benchmarks/generation/signal/whizard/mumu_H_bb_3TeV.sin`.
During the generation process a number of compilation files are created for each subprocess, therefore it is better to run it in a separate folder, like this:

```bash
mkdir gen_Hbb && cd gen_Hbb
whizard ../mucoll-benchmarks/generation/signal/whizard/mumu_H_bb_3TeV.sin
```

This process can be time-consuming, therefore to speed up the process you can copy the pre-generated sample and use it in the next step:

```bash
cp /cvmfs/muoncollider.cern.ch/datasets/tutorial_20230705/gen_Hbb/mumu_H_bb_3TeV.hepmc ./
```

or if you don't have access to CVMFS:

```bash
wget https://nbartosi.web.cern.ch/tutorial_20230705/gen_Hbb/mumu_H_bb_3TeV.hepmc
```

### Madgraph

Another popular event generator is `Madgraph5_aMC@NLO`.
A complete tutorial on how to use it can be found in [this presentation](https://indico.cern.ch/event/555228/sessions/203428/attachments/1315471/1970459/tutorial-CMSandATLAS-2016.pdf).

### Particle gun

It is sometimes useful for performance studies to simulate events with single particles (muons, pions, electrons etc) with specific parameters.
A dedicated Python script allows to generate an LCIO file with stable particles, represented by `LCIO::MCParticle` objects.
For example to generate 100 events with 1 muon/event having `p_T = 10 GeV` and polar angle randomly distributed in the range `[10deg, 170deg]`:

```bash
python ../mucoll-benchmarks/generation/pgun/pgun_lcio.py -e 100 -p 1 --pdg -13 13 --pt 10 --theta 10 170 -- pgun_mu.slcio
```

> NOTE: A complete list of supported PDG IDs is defined in the `PDG_PROPS` dictionary in file `mucoll-benchmarks/generation/pgun/pdgs.py`, which follow the Monte Carlo numbering scheme from [Particle Data Group](https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf).
