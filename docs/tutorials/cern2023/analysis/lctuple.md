# LCTuple

The LCTuple is a configurable flat ROOT tree, produced from the collections in the LCIO files.
Many examples of Marlin steering files to produce the LCTuple and of ROOT macros to access the LCTuple may be found in the [LCTuple repository](https://github.com/MuonColliderSoft/LCTuple).
In the following, we provide the essential information to get quickly started with the LCTuple.


**LCTuple format and variable naming**

Each LCIO collection that is indicated in the steering file is saved as a different branch of the LCTuple tree. The convention for the LCTuple variable names is to use five-letter names, where a two-letter prefix refers to the branch and the following three letters indicate the specific variable.
At first, such a naming convention may appear very cryptic. The quickest way to decode the variable names is to go directly to the LCTuple [source code](https://github.com/MuonColliderSoft/LCTuple/tree/master/src).
A useful vademecum with information on the MC particles, the tracker hits and the reconstructed tracks may be found [here](https://atlaswiki.lbl.gov/analysis/muoncollider/lctuple).


**LCTuple production**

The LCTuple package consists of a Marlin processor that is configured via an xml steering file. An example of a steering file to produce an LCTuple is [lctuple_steer.xml](https://github.com/MuonColliderSoft/MuC-Tutorial/blob/master/analysis/lctuple/lctuple_steer.xml).
The essential information to be provided to the LCTuple processor is:
- the names of the LCIO collections to be dumped into the ROOT tree;
- the input filename;
- the output filename.

To produce the ntuple file lctuple_example.root from the LCIO input_file.slcio with the steering file lctuple_steer.xml:
```
Marlin --global.LCIOInputFiles=input_file.slcio --MyAIDAProcessor.FileName=lctuple_example lctuple_steer.xml
```
In the example above, the input and output filenames are conveniently passed to Marlin as command-line options. The produced LCTuple contains branches for the generator-level Monte Carlo particles, the reconstructed particles, the reconstructed jets, and the event primary verices.


**LCTuple access**

Being the LCtuple a flat ROOT tree, the LCTuple files may be directly opened with ROOT and quickly inspected with the TBrowser.

Of course, a full-fledged analysis requires a more sophisticated approach. An example of a ROOT macro to access and analyse an LCTuple may be found here: [lctuple_analysis.C](https://github.com/MuonColliderSoft/MuC-Tutorial/blob/master/analysis/lctuple/lctuple_analysis.C).

```
root -l lctuple_analysis.C+
```
