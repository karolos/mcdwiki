# Useful Tools
The default output format of the reconstruction package is an [LCIO](https://github.com/iLCSoft/LCIO) (Linear Collider I/O)  file. You can inspect these files using these commands:

1) To print the list of saved collection and their sizes use:
```bash
anajob out.slcio
```

2) To see the details of all the collection of the first event:
```bash
dumpevent out.slcio 1 > event1.txt
```

If you are interest only in the number of events saved use:
```bash
lcio_event_counter out.slcio
```

You can also merge or split an LCIO files:
```bash
$ lcio_merge_files -h

usage: lcio_merge_files <output-file> <input-file1> [[input-file2],...]

$ lcio_split_file -h

usage: lcio_splitfile infilename outfilename sizeInBytes
```
