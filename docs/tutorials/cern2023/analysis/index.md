# Analysis methods

Output of the detector simulation can be analysed at various stages of the workflow using different approaches:

* analysing objects stored in LCIO files using a Marlin processor;
* analysing objects stored in LCIO files using a Python script;
* converting basic objects to a ROOT tuple and analysing it with common analysis tools for ROOT trees.

In the following sections different methods are described in more details.
