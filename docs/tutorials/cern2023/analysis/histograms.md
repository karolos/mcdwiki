# Histograms and other ROOT-based outputs

ILCsoft supports writing output files in native root format, via its built-in AIDAProcessor.
Under the hood, this is done thanks to [RAIDA](https://github.com/iLCSoft/RAIDA). RAIDA stands for Root implementation of AIDA(Abstract Interfaces for Data Analysis). All ROOT objects created with AIDA are stored in a ROOT file. Only objects, which natively exist within ROOT can be created.

In order to produce these outputs you need to include an AIDAprocessor in your steering file.
```xml
<processor name="MyAIDAProcessor" type="AIDAProcessor">
 <!-- Processors only need to create and fill the histograms, clouds and tuples. -->
 <!-- Needs to be the first ActiveProcessor.                                     -->
 <!-- compression of output file 0: false >0: true (default) -->
 <parameter name="Compress" type="int" value="1"/>
 <!-- filename without extension -->
 <parameter name="FileName" type="string" value="lctuple_example"/>
 <!-- type of output file: root-->
 <parameter name="FileType" type="string" value="root "/>
</processor>
```

If the Marlin processors included in your steering file can write histograms or trees, they will be written to the specified file.
One frequently used processor that writes TTrees to an output ROOT file is the [LCTuple](lctuple.md) processor.

# Adding ROOT outputs to your processor
Let's add a TH1F to the output of a generic processor.

To do this, you need to:
1) include the ROOT object class and define a data member in your processor class header file
```c
#include <TH1F.h>
```
[...]
```c
TH1F *my_histogram = nullptr;
```

2) include the AIDAProcessor in your processor implementation.
```c
#include <marlin/AIDAProcessor.h>
```

3) in Processor::init(), initialize the AIDAProcessor and book the histogram.
```c
AIDAProcessor::histogramFactory(this);
my_histogram = new TH1F("my_histo", "A 5 bin TH1F", 5, 0., 5.);
```

4) fill the histograms within Processor::processEvent()
```c
my_histogram->Fill(0);
```

All registered ROOT objects will be written to disk, you don't need to explicitly call the Write() function anywhere.







