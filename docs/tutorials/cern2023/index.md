# Muon Collider Software Tutorial

This wiki serves the purpose of getting people started with the main aspects of the Muon Collider detector-simulation software.
The software suite is largely based on CLIC's [ILCSoft](https://github.com/iLCSoft), with some additions and modifications collected in the dedicated [GitHub repository](https://github.com/MuonColliderSoft/).

> NOTE: We are currently transitioning to a new software stack called [Key4hep](https://key4hep.github.io/key4hep-doc/).
> Therefore some instructions in this tutorial will look differently from the past versions of this tutorial or instructions you might find for the original ILCSoft distributions.

The main focus is running a full-chain Geant4-based simulation, digitization and reconstruction, as well as making performance studies.
A few more advanced topics are listed separately and can be then approached in any order, depending on your specific interest.

### General sequence

Generally a full-simulation study at the Muon Collider includes the following steps:

* **generation:** creating a sample of stable particles (particle gun, Monte Carlo event generator);
* **simulation:** simulating the interaction of stable particles with the detector material;
* **digitisation:** converting energy deposits in the detector to realistic detector signals;
* **reconstruction:** reconstructing individual particles and higher-level objects using dedicated algorithms;

Finally analysis of the output can be performed at any stage for evaluating performance of the detector or of the corresponding reconstruction algorithms.
