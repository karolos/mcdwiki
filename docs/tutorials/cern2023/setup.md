# Computing setup

Detector-simulation software of the **Muon Collider Detector and Physics Group** can be set up in three different ways:

* load the environment pre-installed under the dedicated CVMFS repository (CERN Virtual Machine File System);
* download and run a compiled Docker or Apptainer image;
* manually install all the software on your machine using Spack package manager, following [the instructions](https://github.com/MuonColliderSoft/mucoll-spack).

Each method has its own optimal use cases, but for the purpose of this tutorial we will use the CVMFS installation on on CERN machines running **Alma Linux 9**. 

## Setting up the environment from CVMFS

To set up a working software environment first log in to a compatible machine from the CERN computing cluster, where `${USER}` is the username of your CERN computing account:

```bash
ssh ${USER}@lxplus9.cern.ch
```

> NOTE: we are using the `lxplus9` endpoint instead of the default `lxplus` in order to connect specifically to a machine running `Alma Linux 9`. Standard `lxplus` machines are running `CentOS 8` distribution at the moment of writing this tutorial.

When connected, create a working directory that you will use for the tutorial.
Then source the setup script to obtain the latest mucoll-stack release from `/cvmfs/muoncollider.cern.ch/release/`.

```bash
# Create a working directory
mkdir -p ~/work/mucoll-tutorial-2023
cd ~/work/mucoll-tutorial-2023
# Link the environment-setup script for easy access
ln -s /cvmfs/muoncollider.cern.ch/release/2.8-patch2/setup.sh ./
# Set up the environment
source setup.sh
```

## Reference configuration files

A collection of reference configuration files and analysis tools used by the collaboration are maintained under the [`mucoll-benchmarks`](https://github.com/MuonColliderSoft/mucoll-benchmarks.git) repository.
We will use them at different stages of the tutorial, therefore download the repository using `git`:

```bash
git clone https://github.com/MuonColliderSoft/mucoll-benchmarks.git
```

At every stage of the tutorial we assume that you start from your working directory, which contains at least the following two items:

```bash
mucoll-benchmarks
setup.sh -> /cvmfs/muoncollider.cern.ch/release/2.8-patch2/setup.sh
```

## Reference input files

Each stage of the tutorial produces output that is used as input for the subsequent stages.
Some steps such as generation and GEANT4 simulation can take from tens of minutes to several hours, which is not practical at the limited time scale of this tutorial. Therefore, relevant output files of each stage have been prepared, which you can copy from one of the following locations:

* `/cvmfs/muoncollider.cern.ch/datasets/tutorial_20230705/` - accessible only from **`lxplus`** or machines with configured CVMFS;
* [`https://nbartosi.web.cern.ch/tutorial_20230705/`](https://nbartosi.web.cern.ch/tutorial_20230705/) - accessible from **anywhere**.


## Container Image
The Muon Collider simulation and reconstruction code is available as a Docker image. It is hosted by CERN's Container Registry.
```
gitlab-registry.cern.ch/muon-collider/mucoll-deploy/mucoll:2.8-patch2-el9
```

The setup script is available under `/opt/setup_mucoll.sh`.
```bash
source /opt/setup_mucoll.sh
```

### Apptainer
A typical way to start the container can look like the following:
```bash
apptainer shell docker://gitlab-registry.cern.ch/muon-collider/mucoll-deploy/mucoll:2.8-patch2-el9
```

Then you need to source the environment setting:
```bash
source /opt/setup_mucoll.sh
```

For further options, see the general [apptainer documentation](https://docs.sylabs.io/guides/3.5/user-guide/introduction.html).

> NOTE: The image ill be made available at `/cvmfs/unpacked.cern.ch` in the future.

### Docker
For generic details about the Docker setup please refer to the [guide](https://docs.docker.com/engine/install/).

A typical docker command to start the muon collider software, after having downloaded the image, will look like the following:
```bash
docker run -ti --rm --env DISPLAY=${DISPLAY} --env USER=${USER} --env HOME=/home/${USER} --user=$(id -u $USER):$(id -g $USER) -v <your working directory>:/home/${USER} -v /cvmfs:/cvmfs -w /home/${USER} -v ${HOME}/.Xauthority:/home/${USER}/.Xauthority --net=host --entrypoint /bin/bash gitlab-registry.cern.ch/muon-collider/mucoll-deploy/mucoll:2.8-patch2-el9
```

First we define some environment variables used to export the graphical interface: `--env DISPLAY=${DISPLAY} --env USER=${USER} --env HOME=/home/${USER}`. 
We define also the local user as the user logged in the container (otherwise you login as root):  `--user=$(id -u $USER):$(id -g $USER)`.
We mount some local directories inside the container: `-v <your working directory>:/home/${USER} -v /cvmfs:/cvmfs`.
We mount some local directories inside the container: `-v <your working directory>:/home/${USER} -v /cvmfs:/cvmfs`.
With the next option we define the working directory inside the container:  `-w /home/${USER}`.
The last two options tell  the container to share the host’s networking namespace: `-v ${HOME}/.Xauthority:/home/${USER}/.Xauthority --net=host`.

Then you need to source the environment setting:
```bash
source /opt/setup_mucoll.sh
```

### Shifter
You can convert the Docker image to a shifter image using a simple command:
```bash
shifterimg -v pull docker:gitlab-registry.cern.ch/muon-collider/mucoll-deploy/mucoll:2.8-patch2-el9
```

This only has to be done by one person in the system and the image is usually available to other users as well.

A typical command to then run the image will look like:
```bash
shifter --image=docker:gitlab-registry.cern.ch/muon-collider/mucoll-deploy/mucoll:2.8-patch2-el9 --volume=/global/cfs/cdirs/atlas/spgriso/MuonCollider/data:/data -- /bin/bash --init-file ${HOME}/scripts/bashrc_muc.sh
```
where the `/global/...` path is a path where data is stored and will be visible as `/data` in the container, and `${HOME}/scripts/bashrc_muc.sh` is your personal bashrc script you'd like to run within the container. For example a good thing to do there is to always setup ilcsoft:
```bash
source /opt/setup_mucoll.sh
```
that otherwise needs to be done manually, similarly to the case of other containers. -->

<!-- ## Instructions for specific clusters
In this section we report for convenience instructions for specific computing clusters.

### Snowmass Connect
First, you'll need an account on this cluster. To get one, register at <https://connect.snowmass21.io>. You'll need to edit your profile and add an SSH key for logging into the login node below.

You can then login using ssh:
```bash
ssh login.snowmass21.io #as usual, append your 'your_username@' in case it differs from your local computer account
```

To work efficiently on this cluster, please use the `/work/$USER` folder instead of your home folder. This ensure a faster I/O bandwidth and better user experience for everyone. This is also reflected in the singularity setup below.

You can then load and run the singularity container you can just execute:
```bash
singularity run -B /collab/project/snowmass21/data/muonc:/data -B /work/$USER /cvmfs/unpacked.cern.ch/registry.hub.docker.com/infnpd/mucoll-ilc-framework\:1.6-centos8
```
The "bind" option is used to to mount a working directory `/work/$USER` and the `/data` directory with some commonly useful data.

You can check that the Muon Collider software is properly set up. If everything is OK, you should be able to run the following commands with no error messages:
```bash
cd /work/$USER/
ddsim -h
Marlin -h
```

More information on this cluster is available on its [wiki page](https://maniaclab.uchicago.edu/snowmass-connect-docs/). -->


## Additional Tutorial Setup
For some parts of the tutorial we will also use code that is available in the [MuC-Tutorial](https://github.com/MuonColliderSoft/MuC-Tutorial.git) repository. Clone it in your working directory after setting up the software.

```bash
git clone https://github.com/MuonColliderSoft/MuC-Tutorial.git
```

