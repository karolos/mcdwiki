# Detector simulation

The simulation program `ddsim` is part of the [DD4Hep](https://dd4hep.web.cern.ch/dd4hep/) package, which uses as main components the [ROOT](http://root.cern.ch/) geometry package, which is used for construction and visualization of geometry, and the [Geant4](http://geant4.cern.ch/) simulation toolkit, which can be interfaced via DD4hep to perform detector simulation in complex detector designs.

To run `ddsim` with a specific configuration file the following command should be used:

```bash
ddsim --steeringFile <path_to_the_steering_file>.py
```

Configuration file is a simple python script that configures an instance of the `DD4hepSimulation` object.
For example the path to the detector geometry, assigned to the `compactFile` parameter, is extracted from the `$MUCOLL_GEO` environment variable, which holds the path to the default geometry of the software release:

```python
SIM.compactFile = os.environ.get('MUCOLL_GEO')
```

You can check the actual path of the geometry in the terminal:

```bash
echo $MUCOLL_GEO
```

Any parameter defined in the steering file can be overriden via a command-line parameter.
To see a complete list of available command-line parameters:

```bash
ddsim --help
```

Output of a simulation job is stored in the LCIO format and contains several kinds of objects:

* input `MCParticles`;
* potential secondary `MCParticles` created during the interaction with detector material;
* `SimHits` representing energy deposits of individual `MCParticles` in the detector.

A number of different `SimHit` classes are available to represent the most relevant information for each type of subdetector, such as `SimTrackerHit`, `SimCalorimeterHit`, etc.
In order to check the collections in the simulation output file:
```bash
anajob output_sim.slcio 
```

It is also possible to visually check the simulated events with the event display:
```bash
ced2go -d $MUCOLL_GEO output_sim.slcio        
```  

## Simulating H -> bb events

First create a dedicated folder for the simulation stage:

```bash
mkdir sim_Hbb && cd sim_Hbb
```

To simulate 10 `H -> bb` events from the sample generated in the previous step using the baseline configuration:

```bash
ddsim --steeringFile ../mucoll-benchmarks/simulation/ilcsoft/steer_baseline.py \
--inputFile ../gen_Hbb/mumu_H_bb_3TeV.hepmc \
--numberOfEvents 10 \
--outputFile output_sim.slcio
```

Alternatively you can modify the relevant parameters directly in the steering file, and execute it without any additional command-line parameters:

```bash
ddsim --steeringFile ../mucoll-benchmarks/simulation/ilcsoft/steer_baseline.py
```

## Simulating single particles

Instead of storing particles to a file and then simulating them, it is also possible to generate them directly in `ddsim` as configured in the `SIM.gun` component.
To activate the particle-gun the following parameter has to be enabled:

```python
SIM.enableGun = True
```

Type of the generated particles and their kinematic distributions are configured like this:

```python
SIM.gun.particle = "mu-" # muon
SIM.gun.energy = 10.0*GeV  # fixed energy at 10 GeV
SIM.gun.distribution = "uniform"  # flat in theta distribution    

SIM.gun.isotrop = True # isotropic distribution in Phi
SIM.gun.multiplicity = 1 # one muon per event
SIM.gun.phiMax = None
SIM.gun.phiMin = None
SIM.gun.thetaMax = 180*deg
SIM.gun.thetaMin = 10*deg
```

You can also define parameters of the beamspot for your particle gun sample, where you provide vertex sigma in `cm`.
To get the correct beamspot position for the COM energy you are trying to simulate, you can refer to this accelerator table: [https://muoncollider.web.cern.ch/design/general-parameters](https://muoncollider.web.cern.ch/design/general-parameters)
```python
SIM.vertexSigma = [0.0, 0.0, 0.0, 0.0]
```
