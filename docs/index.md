# Muon Collider Detector and Physics Wiki

This Wiki page collects all the essential information to start using the detector-simulation and physics-performance tools of the [International Muon Collider Collaboration](https://muoncollider.web.cern.ch/){target=_blank} (IMCC).

## Support channels
* Detector and Physics Simulation: [muoncollider-detector-physics@cern.ch](mailto:muoncollider-detector-physics@cern.ch)
* Software Issues: [muon_collider_software@lists.infn.it](mailto:muon_collider_software@lists.infn.it)

## Meetings

### Working Group Meetings
* [Physics Potential](https://indico.cern.ch/category/12792/)
* [Physics and Detector Studies](https://indico.cern.ch/category/13145/)
* [Synergies](https://indico.cern.ch/category/17069/)

### Annual Collaboration Meetings
* [IMCC Annual Meeting 2023](https://indico.cern.ch/event/1250075/timetable/)
* [IMCC Annual Meeting 2022](https://indico.cern.ch/event/1175126/timetable/)

## Past tutorial events
* [CERN MuCol training, Jul 5-6th 2023](https://indico.cern.ch/event/1277924/timetable/){target=_blank}
* [Fermilab Workshop, Dec 16th 2022](https://indico.fnal.gov/event/56615/timetable/){target=_blank}
* [Snowmass '21, Sep 30th 2020](https://indico.fnal.gov/event/45187/timetable/#20200930.detailed){target=_blank}

