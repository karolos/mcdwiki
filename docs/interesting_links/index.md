# Userful Links

## Introductory and reference material

Geared towards people getting started. See also [here](https://confluence.infn.it/display/muoncollider/Articles+and+links) for a great set of papers on the topic.

- [Nature Physics volume 17, pages289–292 (2021)](https://www.nature.com/articles/s41567-020-01130-x): A useful introductory article to the motivations for a Muon Collider.
- [arXiv:2203.07964](https://arxiv.org/abs/2203.07964): Snowmass Muon Collider Detector Performance.
- [arXiv:2209.01318](https://arxiv.org/abs/2209.01318): Snowmass Muon Collider Detector Technology.
- [arXiv:2303.08533](https://arxiv.org/abs/2303.08533): Review on status of Muon Collider accelerator and detector studies.

For those who like videos, a few general audience talks.

- [Nathaniel Craig Colloquium @ Princeton](https://www.kaltura.com/index.php/extwidget/preview/partner_id/1449362/uiconf_id/14292362/entry_id/1_uk6n89mu/embed/dynamic?): Talk on the physics case of a Muon Collider
- [Nima Arkani-Hamed @ UCSB](https://online.kitp.ucsb.edu/online/bblunch/arkanihamed4/): Very broad audience chalk talk on Muon Colliders
- [KITP Muon Collider Workshop](https://online.kitp.ucsb.edu/online/muoncollider-m23/): Collection of talks meant for a broad audience, many focusing on detector and accelerator R&D status at an approachable level
